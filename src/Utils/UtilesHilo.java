package Utils;

/**
 *
 * @author Alejandro Younes
 */
public class UtilesHilo {

    public static String nombre() {
        return Thread.currentThread().getName();
    }

    public static void simularActividad(int milisegundos) {
        try {
            Thread.sleep(milisegundos);
        } catch (InterruptedException ex) {
        }
    }

    public static void simularActividad(int milisegundos, String actividad) {
        System.out.println(nombre() + ": empieza a " + actividad);
        simularActividad(milisegundos);
        System.out.println(nombre() + ": termina de " + actividad);
    }
}
