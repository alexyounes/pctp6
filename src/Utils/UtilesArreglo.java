package Utils;

/**
 *
 * @author Alejandro Younes
 */
public class UtilesArreglo {

    // Obtener posicion de num recorriendo de forma circular
    // un arreglo de n elementos
    public static int modulo(int num, int n) {
        if (num < 0) {
            num = n + num;
        }
        return num % n;
    }
}
