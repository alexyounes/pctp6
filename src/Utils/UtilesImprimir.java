package Utils;

/**
 *
 * @author Alejandro Younes
 */
public class UtilesImprimir {

    public enum Color {
        NEGRO("\u001B[30m"),
        ROJO("\u001B[31m"),
        VERDE("\u001B[32m"),
        AMARILLO("\u001B[33m"),
        AZUL("\u001B[34m"),
        VIOLETA("\u001B[35m"),
        CELESTE("\u001B[36m"),
        GRIS("\u001B[37m"),
        FONDOROJO("\u001B[41m"),
        FONDOVERDE("\u001B[42m"),
        FONDOAMARILLO("\u001B[43m"),
        FONDOAZUL("\u001B[44m"),
        FONDOVIOLETA("\u001B[45m"),
        FONDOCELESTE("\u001B[46m"),
        FONDOGRIS("\u001B[47m");

        private final String valor;

        private Color(String valor) {
            this.valor = valor;
        }
    }

    private static final Color[] COLORES = Color.values();

    public static void imprimirLineaColor(String cadena) {
        String nombreHilo = Thread.currentThread().getName();
        char caracter = nombreHilo.charAt(nombreHilo.length() - 1);
        int numColor = Integer.parseInt(String.valueOf(caracter));
        Color color = COLORES[numColor];
        System.out.println(color.valor + cadena + "\u001B[0m");
    }

    public static void imprimirLineaColor(String cadena, Color color) {
        System.out.println(color.valor + cadena + "\u001B[0m");
    }

    public static void imprimirLineaColor(char caracter, Color color) {
        System.out.println(color.valor + caracter + "\u001B[0m");
    }

    public static void imprimirColor(String cadena, Color color) {
        System.out.print(color.valor + cadena + "\u001B[0m");
    }

    public static void escribirColor(char caracter, Color color) {
        System.out.print(color.valor + caracter + "\u001B[0m");
    }
}
