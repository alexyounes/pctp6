package E5_Mon;

import static Utils.UtilesHilo.nombre;

/**
 *
 * @author Alejandro Younes
 */
public class PuenteMon {

    int sentidoActual = -1; // -1: Ninguno, 0: Sur, 1: Norte
    int autosEntrados = 0; // Cantidad total de autos que entraron
    int numSalida = 0; // Numero del auto que tiene que salir

    public synchronized int entrarCocheDelNorte() throws InterruptedException {
        while (sentidoActual == 0) { // Mientras venga el sentido contrario
            wait(); // Espera a que le avisen
        }
        sentidoActual = 1; // Indica su sentido
        int numEntrada = autosEntrados; // Obtiene su numero
        autosEntrados++; // Deja numero para el auto siguiente
        System.out.println(nombre() + "ENTRA al puente por el NORTE");
        return numEntrada;
    }

    public synchronized void salirCocheDelNorte(int numEntrada) throws InterruptedException {
        while (numEntrada != numSalida) { // Si se adelanto
            wait(); // Espera que le avisen
        }
        if (numSalida + 1 == autosEntrados) { // Si es el ultimo auto
            autosEntrados = 0; // Reinicia la cantidad de autos que entraron
            numSalida = 0; // Reinicia el numero de auto que sale
            sentidoActual = -1; // Indica que no pasan autos
        } else { // Si no es el ultimo auto
            numSalida++; // Deja el numero del auto siguiente
        }
        System.out.println(nombre() + "SALE del puente por el NORTE");
        notifyAll(); // Avisa a los autos esperando
    }

    public synchronized int entrarCocheDelSur() throws InterruptedException {
        while (sentidoActual == 1) { // Mientras venga el sentido contrario
            wait(); // Espera a que le avisen
        }
        sentidoActual = 0; // Indica su sentido
        int numEntrada = autosEntrados; // Obtiene su numero
        autosEntrados++; // Deja numero para el auto siguiente
        System.out.println(nombre() + "ENTRA al puente por el SUR");
        return numEntrada;
    }

    public synchronized void salirCocheDelSur(int numEntrada) throws InterruptedException {
        while (numEntrada != numSalida) { // Si se adelanto
            wait(); // Espera que le avisen
        }
        if (numSalida + 1== autosEntrados) { // Si es el ultimo auto
            autosEntrados = 0; // Reinicia la cantidad de autos que entraron
            numSalida = 0; // Reinicia el numero de auto que sale
            sentidoActual = -1; // Indica que no pasan autos
        } else { // Si no es el ultimo auto
            numSalida++; // Deja el numero del auto siguiente
        }
        System.out.println(nombre() + "SALE del puente por el SUR");
        notifyAll(); // Avisa a los autos esperando
    }
}
