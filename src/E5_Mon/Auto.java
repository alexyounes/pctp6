package E5_Mon;

/**
 *
 * @author Alejandro Younes
 */
public class Auto implements Runnable {

    private final PuenteMon puente;
    private final boolean delNorte;
    private int orden;

    public Auto(PuenteMon puente, boolean delNorte) {
        this.puente = puente;
        this.delNorte = delNorte;
    }

    @Override
    public void run() {
        if (delNorte) {
            try {
                orden = puente.entrarCocheDelNorte();
                Thread.sleep(2000);
                puente.salirCocheDelSur(orden);
            } catch (InterruptedException ex) {
            }
        } else {
            try {
                orden = puente.entrarCocheDelSur();
                Thread.sleep(2000);
                puente.salirCocheDelNorte(orden);
            } catch (InterruptedException ex) {
            }
        }
    }
}
