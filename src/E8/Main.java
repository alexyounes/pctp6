package E8;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        int camillas = 4; // 4
        int revistas = 9; // 9
        int asientos = 12; // 12
        int personas = 50;
        Centro centro = new Centro(camillas, revistas, asientos);
        Persona[] persona = new Persona[personas];
        Thread[] hilo = new Thread[personas];
        for (int i = 0; i < personas; i++) {
            persona[i] = new Persona(centro);
            hilo[i] = new Thread(persona[i], "Persona " + i + ": ");
            hilo[i].start();
        }
    }
}
