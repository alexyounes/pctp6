package E8;

import static Utils.UtilesHilo.nombre;
import Utils.UtilesImprimir.Color;
import static Utils.UtilesImprimir.imprimirLineaColor;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Centro {

    private int revDis;
    private int camDis;
    private int silDis;
    private int turnoActual = 0;
    private int turnosTotales = 0;

    private final ReentrantLock mutex = new ReentrantLock();
    private final Condition camilla = mutex.newCondition();
    private final Condition revistaTurno = mutex.newCondition();
    private final Random seSienta = new Random();

    public Centro(int camillas, int revistas, int sillas) {
        this.camDis = camillas;
        this.revDis = revistas;
        this.silDis = sillas;
    }

    public int obtenerTurno() {
        int turno;
        mutex.lock();
        turno = turnosTotales; // Obtiene su numero de turno
        turnosTotales++; // Deja listo el proximo numero de turno
        mutex.unlock();
        return turno;
    }

    public void ocuparCamilla(int turno) throws InterruptedException {
        mutex.lock();
        imprimirLineaColor(nombre() + "llega al centro", Color.VERDE);
        while (camDis == 0) { // Mientras no haya camilla
            if (silDis > 0 && seSienta.nextBoolean()) { // Si quiere sentarse y hay silla disponible
                System.out.println(nombre() + "ocupa una silla");
                silDis--; // Se sienta hasta que lo llamen
                esperarCamilla(turno); // Espera que lo llamen por su turno
                System.out.println(nombre() + "desocupa la silla");
                silDis++; // Se levanta para ver si hay una camilla
            } else {
                System.out.println(nombre() + "espera parado");
                esperarCamilla(turno); // Espera que lo llamen por su turno
            }
        }
        camDis--; // Ocupa una camilla
        imprimirLineaColor(nombre() + "usa la camilla", Color.VIOLETA);
        mutex.unlock();
    }

    private void esperarCamilla(int turno) throws InterruptedException {
        boolean conRevista = leerRevista(turno); // Trata de obtener revista
        while(turnoActual != turno){ // Mientras no sea su turno
        camilla.await(); // Espera la camilla
        }
        if (conRevista) { // Si tenia revista cuando lo llaman
            dejarRevista(); // La deja
        }
    }

    public void desocuparCamilla() {
        mutex.lock();
        camilla.signal(); // Avisa que desocupo la camilla
        revistaTurno.signalAll(); // Avisa a los que esperan una revista
        camDis++; // Indica que hay una camilla disponible mas
        turnoActual++; // Indica que es el turno del siguiente
        System.out.println(nombre() + "desocupa la camilla");
        mutex.unlock();
    }

    private boolean leerRevista(int turno) throws InterruptedException {
        boolean conRevista;
        while (revDis == 0 && turnoActual != turno) { // Si no hay una revista y no es su turno
            System.out.println(nombre() + "ve noticiero");
            revistaTurno.await(); // Espera a que se desocupe una revista o sea su turno
            System.out.println(nombre() + "intenta obtener revista");
        }
        if (turnoActual != turno) { // Si no es su turno pero se libero por la revista
            System.out.println(nombre() + "lee revista");
            revDis--; // Agarra una revista
            conRevista = true; // Indica que tiene una revista
        } else { // Si es su turno
            conRevista = false; // Indica que no tiene una revista
        }
        return conRevista;
    }

    private void dejarRevista() {
        System.out.println(nombre() + "deja revista");
        revDis++; // Deja la revista
        revistaTurno.signalAll(); // Avisa a todos que dejo la revista
    }
}
