package E8;

/**
 *
 * @author Alejandro Younes
 */
public class Persona implements Runnable {

    private final Centro centro;
    private int turno;

    public Persona(Centro centro) {
        this.centro = centro;
    }

    @Override
    public void run() {
        try {
            turno = centro.obtenerTurno();
            centro.ocuparCamilla(turno);
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        centro.desocuparCamilla();
    }

}
