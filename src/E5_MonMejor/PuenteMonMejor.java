package E5_MonMejor;

import static Utils.UtilesHilo.nombre;

/**
 *
 * @author Alejandro Younes
 */
public class PuenteMonMejor {

    int sentidoActual = -1; // -1: Ninguno, 0: Sur, 1: Norte
    int autosNorte = 0; // Cantidad total de autos del norte
    int autosSur = 0; // Cantidad total de autos del norte
    int numSalida = 0; // Numero del auto que tiene que salir

    public synchronized void entrarCocheDelNorte() throws InterruptedException {
        int numEntrada = autosNorte; // Obtiene su numero
        autosNorte++; // Deja numero para el auto siguiente
        while (sentidoActual == 0 || numEntrada != numSalida) { // Mientras venga el sentido contrario
            wait(); // Espera a que le avisen
        }
        sentidoActual = 1; // Indica su sentido
        System.out.println(nombre() + "ENTRA al puente por el NORTE");
        numSalida++; // Cambia numero para el auto siguiente
        notifyAll(); // Avisa a los autos esperando
    }

    public void salirCocheDelNorte(int numEntrada) throws InterruptedException {
        salirCoche(1);
    }

    public synchronized void entrarCocheDelSur() throws InterruptedException {
        int numEntrada = autosSur; // Obtiene su numero
        autosSur++; // Deja numero para el auto siguiente
        while (sentidoActual == 1 || numEntrada != numSalida) { // Mientras venga el sentido contrario
            wait(); // Espera a que le avisen
        }
        sentidoActual = 0; // Indica su sentido
        System.out.println(nombre() + "ENTRA al puente por el SUR");
        numSalida++;
        notifyAll();
    }

    public void salirCocheDelSur(int numEntrada) throws InterruptedException {
        salirCoche(0);
    }

    /*
    public synchronized void entrarCoche(int sentido, int opuesto) throws InterruptedException {
        int numEntrada = autosEntrados; // Obtiene su numero
        autosEntrados++; // Deja numero para el auto siguiente
        while (sentidoActual == opuesto || numEntrada != numSalida) { // Mientras venga el sentido contrario
            wait(); // Espera a que le avisen
        }
        sentidoActual = sentido; // Indica su sentido
        if (sentido == 1) {
            System.out.println(nombre() + "ENTRA al puente por el NORTE");
        } else {
            System.out.println(nombre() + "ENTRA al puente por el SUR");
        }
        numSalida++;
        notifyAll();
    }
     */
    public synchronized void salirCoche(int sentido) throws InterruptedException {
        if (sentido == 0) {
            if (numSalida + 1 == autosSur) { // Si es el ultimo auto
                autosSur = 0; // Reinicia la cantidad de autos que entraron
                numSalida = 0; // Reinicia el numero de auto que sale
                sentidoActual = -1; // Indica que no pasan autos
            }
            System.out.println(nombre() + "SALE por el NORTE");
        } else {
            if (numSalida + 1 == autosNorte) { // Si es el ultimo auto
                autosNorte = 0; // Reinicia la cantidad de autos que entraron
                numSalida = 0; // Reinicia el numero de auto que sale
                sentidoActual = -1; // Indica que no pasan autos
            }
            System.out.println(nombre() + "SALE por el SUR");
        }
        notifyAll(); // Avisa a los autos esperando
    }
}
