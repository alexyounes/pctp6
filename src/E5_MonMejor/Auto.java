package E5_MonMejor;

/**
 *
 * @author Alejandro Younes
 */
public class Auto implements Runnable {

    private final PuenteMonMejor puente;
    private final boolean delNorte;
    private int orden;

    public Auto(PuenteMonMejor puente, boolean delNorte) {
        this.puente = puente;
        this.delNorte = delNorte;
    }

    @Override
    public void run() {
        if (delNorte) {
            try {
                puente.entrarCocheDelNorte();
                Thread.sleep(2000);
                puente.salirCocheDelSur(1);
            } catch (InterruptedException ex) {
            }
        } else {
            try {
                puente.entrarCocheDelSur();
                Thread.sleep(2000);
                puente.salirCocheDelNorte(0);
            } catch (InterruptedException ex) {
            }
        }
    }
}
