package E5_MonMejor;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        int autosDelNorte = 10;
        int autosDelSur = 10;
        int contador = 0;
        PuenteMonMejor puente = new PuenteMonMejor();
        Auto[] autoDelNorte = new Auto[autosDelNorte];
        Auto[] autoDelSur = new Auto[autosDelSur];
        Thread[] hilo = new Thread[autosDelNorte + autosDelSur];
        for (int i = 0; i < autosDelNorte; i++) {
            autoDelNorte[i] = new Auto(puente, true);
            hilo[contador] = new Thread(autoDelNorte[i], "Auto Norte " + i + ": ");
            hilo[contador].start();
            contador++;
        }
        for (int i = 0; i < autosDelNorte; i++) {
            autoDelSur[i] = new Auto(puente, false);
            hilo[contador] = new Thread(autoDelSur[i], "Auto Sur " + i + ": ");
            hilo[contador].start();
            contador++;
        }
    }
}

// debe considerar que la implementación asegure que los coches crucen 
// el puente en el orden en que llegaron, es decir, si C1 llegó al puente
// antes que C2, en la misma dirección, C1 debe poder empezar y terminar
// de cruzar el puente antes que C2.