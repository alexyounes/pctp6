package E6;

/**
 *
 * @author Alejandro Younes
 */
public abstract class Persona implements Runnable {

    final Observatorio observatorio;
    int tipo;

    public Persona(Observatorio observatorio) {
        this.observatorio = observatorio;
    }

    abstract void actividad();

    @Override
    public void run() {
        observatorio.entrar(tipo);
        actividad();
        observatorio.salir(tipo);
    }
}
