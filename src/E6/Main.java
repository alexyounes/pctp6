package E6;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        int capNor = 50;
        int capRed = 30;
        int maxSeg = 10;
        Observatorio observatorio = new Observatorio(capNor, capRed, maxSeg);
        int visitantes = 30;
        int visitantesEnSilla = 10;
        int deMantenimiento = 10;
        int investigadores = 10;
        int total = visitantes + visitantesEnSilla + deMantenimiento + investigadores;
        Persona[] persona = new Persona[total];
        Thread[] hilo = new Thread[total];
        int contador = 0;
        for (int i = 0; i < visitantes; i++) {
            persona[i] = new Visitante(observatorio, false);
            hilo[i] = new Thread(persona[i], "Visitante " + i + ": ");
            contador++;
        }
        for (int i = 0; i < visitantesEnSilla; i++) {
            persona[contador] = new Visitante(observatorio, true);
            hilo[contador] = new Thread(persona[contador], "Visitante (Silla) " + i + ": ");
            contador++;
        }
        for (int i = 0; i < deMantenimiento; i++) {
            persona[contador] = new Mantenimiento(observatorio);
            hilo[contador] = new Thread(persona[contador], "Mantenimiento " + i + ": ");
            contador++;
        }
        for (int i = 0; i < investigadores; i++) {
            persona[contador] = new Investigador(observatorio);
            hilo[contador] = new Thread(persona[contador], "Investigador " + i + ": ");
            contador++;
        }
        for (int i = 0; i < contador; i++) {
            hilo[i].start();
        }
    }
}
