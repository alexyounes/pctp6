package E6;

import static E3.UtilesHilo.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Mantenimiento extends Persona {

    public Mantenimiento(Observatorio observatorio) {
        super(observatorio);
        tipo = 1;
    }

    @Override
    void actividad() {
        simularActividad(5000);
    }
}
