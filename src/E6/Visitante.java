package E6;

import static E3.UtilesHilo.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Visitante extends Persona {

    private final boolean silla;

    public Visitante(Observatorio observatorio, boolean silla) {
        super(observatorio);
        this.silla = silla;
        tipo = 0;
    }

    @Override
    void actividad() {
        simularActividad(5000);
    }

    @Override
    public void run() {
        observatorio.entrar(tipo,silla);
        actividad();
        observatorio.salir(tipo,silla);
    }
}
