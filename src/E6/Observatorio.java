package E6;

import static E3.UtilesHilo.nombre;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Observatorio {

    private final int capNor;
    private final int capRed;
    private final int maxSeg;
    private int capAct;
    private int segAct = 0;
    private int perAct = 0;
    private int tipoAct = -1; // -1: Ninguno, 0: Vis, 1: Man, 2: Inv
    private boolean otroEspera = false;

    private final ReentrantLock cerrojo = new ReentrantLock();
    private final Condition[] condicion = new Condition[]{
        cerrojo.newCondition(), // 0: Condicion Visitantes
        cerrojo.newCondition(), // 1: Condicion Mantenimiento
        cerrojo.newCondition()};// 2: Condicion Investigadores

    public Observatorio(int capNor, int capRed, int maxSeg) {
        this.capNor = capNor;
        this.capRed = capRed;
        this.capAct = capNor;
        this.maxSeg = maxSeg;
    }

    public void entrar(int tipo) {
        cerrojo.lock();
        while ((tipoAct != -1 && tipoAct != tipo) || perAct == capAct || (otroEspera && segAct >= maxSeg)) {
            if(tipoAct != tipo){
                otroEspera = true;
            }
            try {
                condicion[tipo].await();
            } catch (InterruptedException ex) {
            }
        }
        segAct++;
        perAct++;
        tipoAct = tipo;
        System.out.println(nombre() + "entra");
        cerrojo.unlock();
    }

    public void entrar(int tipo, boolean enSilla) {
        cerrojo.lock();
        entrar(tipo);
        if (enSilla) {
            capAct = capRed;
        }
        cerrojo.unlock();
    }

    public void salir(int tipo) {
        cerrojo.lock();
        perAct--;
        if (perAct == 0) {
            segAct = 0;
            tipoAct = -1;
            for (int i = 0; i < condicion.length; i++) {
                if (i != tipo) {
                    condicion[i].signalAll();
                }
            }
        }
        System.out.println(nombre() + "sale");
        cerrojo.unlock();
    }

    public void salir(int tipo, boolean enSilla) {
        cerrojo.lock();
        salir(tipo);
        if (enSilla) {
            capAct = capNor;
        }
        cerrojo.unlock();
    }
}
