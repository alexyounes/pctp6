package E6;

import static E3.UtilesHilo.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Investigador extends Persona {
    
    public Investigador(Observatorio observatorio) {
        super(observatorio);
        tipo = 2;
    }

    @Override
    void actividad() {
        simularActividad(5000);
    }
}