package E1;

/**
 *
 * @author Alejandro Younes
 */
public class Fumador implements Runnable {

    private final int id;
    private final SalaFumadores sala;

    public Fumador(int id, SalaFumadores sala) {
        this.id = id;
        this.sala = sala;
    } //constructor

    @Override
    public void run() {
        while (true) {
            try {
                sala.entrafumar(id);
                System.out.println("Fumador " + id + " está fumando.");
                Thread.sleep(1000);
                sala.terminafumar();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } //catch
        } //while
    } //run
} // clase
