package E1;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class SalaFumadores {

    private int ingredienteFaltante = -1;
    private final ReentrantLock fumando = new ReentrantLock();

    public synchronized void entrafumar(int ingrediente) {
        while (ingrediente != ingredienteFaltante) { // Mientras no sea su ingrediente espera
            try {
                this.wait();
            } catch (InterruptedException ex) {
            }
        }
        fumando.lock(); // Evita que otro fumador entre
    }

    public void terminafumar() {
        fumando.unlock(); // Permite que otro fumador entre
    }

    public synchronized void colocar(int ingredienteFaltante) {
        this.ingredienteFaltante = ingredienteFaltante;
        this.notifyAll();
    }
}