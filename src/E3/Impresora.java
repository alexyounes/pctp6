package E3;

import static E3.UtilesHilo.nombre;
import static E3.UtilesHilo.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public abstract class Impresora implements Runnable {

    private final Buffer buffer;

    public Impresora(Buffer buffer) {
        this.buffer = buffer;
    }

    private void imprimirTarea() {
//        System.out.println(nombre() + "quiere imprimir trabajo");
//        System.out.println(nombre() + "empieza");
//        simularActividad(5000);
//        System.out.println(nombre() + "termina");
        buffer.consumirElemento();
    }

    @Override
    public void run() {
        while (true) {
            imprimirTarea();
        }
    }
}
