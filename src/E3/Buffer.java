package E3;

import static E3.UtilesHilo.nombre;
import static E3.UtilesHilo.simularActividad;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Buffer {

    private final int n;
    private final int[] elementos;
    private int punProductor = -1;
    private int punConsumidor = -1;
    private int ocupados = 0;
    private final ReentrantLock mutexOcupados = new ReentrantLock();
    private final Object monConsumidor = new Object();
    private final Object monProductor = new Object();

    public Buffer(int elementos) {
        this.n = elementos;
        this.elementos = new int[this.n];
    }

    public void producirElemento(int elemento) {
        synchronized (monProductor) {
            mutexOcupados.lock();
            while (ocupados == n) {
                mutexOcupados.unlock();
                try {
                    monProductor.wait(); // Quiere llenar un espacio vacio
                } catch (InterruptedException ex) {
                }
                mutexOcupados.lock();
            }
            mutexOcupados.unlock();
        }
        punProductor = (punProductor + 1) % n; // Avanza de forma circular
        // System.out.println(nombre() + ": produce " + elemento + " en " + punProductor);
        elementos[punProductor] = elemento; // marca el espacio
        mutexOcupados.lock();
        ocupados++;
        mutexOcupados.unlock();
        synchronized (monConsumidor) {
            System.out.println("ocupados: " + ocupados);
            System.out.println(nombre() + "agrego trabajo");
            monConsumidor.notifyAll(); // Espacio llenado, avisa a consumidores
        }
    }

    public void consumirElemento() {
        synchronized (monConsumidor) {
            mutexOcupados.lock();
            while (ocupados == 0) {
                mutexOcupados.unlock();
                try {
                    monConsumidor.wait(); // Quiere vaciar un espacio lleno
                } catch (InterruptedException ex) {
                }
                mutexOcupados.lock();
            }
            mutexOcupados.unlock();
        }
        punConsumidor = (punConsumidor + 1) % n; // Avanza de forma circular
        // System.out.println(nombre() + ": consume elemento en " + punConsumidor);
        elementos[punConsumidor] = -1; // Vacia el espacio
        System.out.println(nombre() + "empieza");
        simularActividad(5000);
        System.out.println(nombre() + "termina");
        mutexOcupados.lock();
        ocupados--;
        mutexOcupados.unlock();
        synchronized (monProductor) {
            monProductor.notifyAll(); // Espacio vaciado, avisa
        }
    }
}
