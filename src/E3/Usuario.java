package E3;

import static E3.UtilesHilo.nombre;

/**
 *
 * @author Alejandro Younes
 */
public abstract class Usuario implements Runnable {

    Buffer tipo;

    public Usuario(Buffer tipo) {
        this.tipo = tipo;
    }

    private void agregarTareaImpresion() {
//        System.out.println(nombre() + "quiere agregar trabajo");
        tipo.producirElemento(9);
    }

    @Override
    public void run() {
        while(true){
        agregarTareaImpresion();
        }
    }
}
