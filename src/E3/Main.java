package E3;

public class Main {

    public static void main(String[] args) {
        int i;
        final int numUsuA = 3;
        final int numUsuB = 3;
        final int numUsuAB = 3;
        final int numImpA = 1;
        final int numImpB = 1;
        final int k = 2;
        Buffer bufA = new Buffer(k * numImpA);
        Buffer bufB = new Buffer(k * numImpB);
        Impresora tipoA = new TipoA(bufA);
        Impresora tipoB = new TipoB(bufB);
        Usuario[] usuarioAB = new Usuario[numUsuAB];
        Usuario usuarioA = new UsuarioA(bufA);
        Usuario usuarioB = new UsuarioB(bufB);
        Thread[] hilosUAB = new Thread[numUsuAB];
        Thread[] hilosUA = new Thread[numUsuA];
        Thread[] hilosUB = new Thread[numUsuB];
        Thread[] hilosIA = new Thread[numImpA];
        Thread[] hilosIB = new Thread[numImpB];
        for (i = 0; i < numImpA; i++) {
            hilosIA[i] = new Thread(tipoA, "ImpA " + i + ": ");
            hilosIA[i].start();
        }
        for (i = 0; i < numImpB; i++) {
            hilosIB[i] = new Thread(tipoB, "ImpB " + i + ": ");
            hilosIB[i].start();
        }
        for (i = 0; i < numUsuAB; i++) {
            usuarioAB[i] = new UsuarioAB(bufA, bufB);
            hilosUAB[i] = new Thread(usuarioAB[i], "UsuAB " + i + ": ");
            hilosUAB[i].start();
        }
        for (i = 0; i < numUsuA; i++) {
            hilosUA[i] = new Thread(usuarioA, "UsuA " + i + ": ");
            hilosUA[i].start();
        }
        for (i = 0; i < numUsuB; i++) {
            hilosUB[i] = new Thread(usuarioB, "UsuB " + i + ": ");
            hilosUB[i].start();
        }
    }
}

/*
b. ¿Qué podría suceder si existiera un tercer buffer compartido
por todas las impresoras para los trabajos a los que les sirve
cualquier tipo de impresora?

Tendrían las impresora que consultar de manera alternada cada
buffer, primero en el buffer de su tipo y luego en el general
*/