package E3;

import java.util.Random;

/**
 *
 * @author Alejandro Younes
 */
public class UsuarioAB extends Usuario {

    private final Buffer otroTipo;

    public UsuarioAB(Buffer tipo, Buffer otroTipo) {
        super(tipo);
        this.otroTipo = otroTipo;
        elegirTipo();
    }

    private void elegirTipo() {
        Random aleatorio = new Random();
        int numero = aleatorio.nextInt(2);
        if (numero == 0) {
            System.out.println("Tipo A elegido");
        } else {
            tipo = otroTipo;
            System.out.println("Tipo B elegido");
        }
    }
}
