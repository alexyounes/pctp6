package E7;

/**
 *
 * @author Alejandro Younes
 */
public class Empaquetador implements Runnable {

    private final Mostrador mostrador;
    private int pesoActual;

    public Empaquetador(Mostrador mostrador) {
        this.mostrador = mostrador;
    }

    @Override
    public void run() {
        while (true) {
            pesoActual = mostrador.tomarPastel();
            mostrador.soltarPastel(pesoActual);
        }
    }
}
