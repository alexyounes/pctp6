package E7;

/**
 *
 * @author Alejandro Younes
 */
public class Brazo implements Runnable {

    private final Mostrador mostrador;

    public Brazo(Mostrador mostrador) {
        this.mostrador = mostrador;
    }

    @Override
    public void run() {
        while (true) {
            mostrador.retirarCaja();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
            mostrador.reponerCaja();
        }
    }
}
