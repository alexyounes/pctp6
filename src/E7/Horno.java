package E7;

/**
 *
 * @author Alejandro Younes
 */
public class Horno implements Runnable {

    private final Mostrador mostrador;
    private final int pesoTipo;

    public Horno(Mostrador mostrador, int pesoTipo) {
        this.mostrador = mostrador;
        this.pesoTipo = pesoTipo;
    }

    @Override
    public void run() {
        while (true) {
            mostrador.producirPastel(pesoTipo);
        }
    }
}
