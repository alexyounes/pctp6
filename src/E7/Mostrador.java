package E7;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Mostrador {

    private int cajaAct = 0;
    private int cajaPes = 0;
    private final int maxPes;
    private final int[] espacio;
    private int ocupados = 0;
    private final int n;

    private final ReentrantLock mutex = new ReentrantLock();
    private final Condition lleno = mutex.newCondition();
    private final Condition vacio = mutex.newCondition();
    private final Condition nueva = mutex.newCondition();
    private final Condition cambiar = mutex.newCondition();
    private boolean cajaLlena = false;
    private int punHor = 0;
    private int punEmp = 0;

    public Mostrador(int maxPes, int espacio) {
        this.maxPes = maxPes;
        this.espacio = new int[espacio];
        this.n = espacio;
    }

    public void retirarCaja() {
        mutex.lock();
        while (!cajaLlena) { // Mientras la caja no este llena
            try {
                cambiar.await(); // Espera a que un empaquetador le avise
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("Brazo retira la caja " + cajaAct);
        mutex.unlock();
    }

    public void reponerCaja() {
        mutex.lock();
        System.out.println("Brazo coloca caja nueva");
        cajaAct++;
        cajaPes = 0;
        nueva.signalAll(); // Avisa a los empaquetadores de la caja nueva
        mutex.unlock();
    }

    public int tomarPastel() { // Retorna Peso
        mutex.lock();
        while (ocupados == 0) { // Si no hay mas pasteles
            try {
                vacio.await(); // Espera a que un horno le avise
            } catch (InterruptedException ex) {
            }
        }
        int peso = espacio[punEmp]; // Obtiene un pastel y su peso
        ocupados--; // indica que saco un pastel
        punEmp = (punEmp + 1) % n; // Determina de donde sacara el proximo
        System.out.println("Se obtuvo un pastel de peso " + peso);
        lleno.signalAll(); // Avisa a los hornos
        mutex.unlock();
        return peso; // Recuerda el peso del pastel
    }

    public void soltarPastel(int peso) {
        mutex.lock();
        while ((peso + cajaPes) > maxPes) { // Si sobrepasa el peso
            try {
                cajaLlena = true; // Indica que la caja esta llena
                cambiar.signalAll(); // Avisa al brazo
                nueva.await(); // Espera a que el brazo le avise
            } catch (InterruptedException ex) {
            }
        }
        cajaPes = peso + cajaPes;
        System.out.println("Se solto un pastel de peso " + peso);
        System.out.println("la caja ahora pesa " + cajaPes);
        mutex.unlock();
    }

    public void producirPastel(int peso) {
        mutex.lock();
        while (ocupados == n) { // Si no hay espacio para mas pasteles
            try {
                lleno.await(); // Espera a que un empaquetador le avise
            } catch (InterruptedException ex) {
            }
        }
        espacio[punHor] = peso; // Deja un pastel de su tipo
        ocupados++; // Indica que dejo un pastel
        punHor = (punHor + 1) % n; // Determina donde dejara el proximo
        System.out.println("Se produjo un pastel de peso " + peso);
        vacio.signalAll(); // Avisa a los empaquetadores
        mutex.unlock();
    }
}
