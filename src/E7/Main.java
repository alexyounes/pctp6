package E7;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        int pesoA = 10;
        int pesoB = 20;
        int pesoC = 30;
        int pesoMaximo = 50;
        int espacio = 5;
        Mostrador mostrador = new Mostrador(pesoMaximo, espacio);
        Brazo brazo = new Brazo(mostrador);
        Empaquetador empaque1 = new Empaquetador(mostrador);
        Empaquetador empaque2 = new Empaquetador(mostrador);
        Horno hornoA = new Horno(mostrador, pesoA);
        Horno hornoB = new Horno(mostrador, pesoB);
        Horno hornoC = new Horno(mostrador, pesoC);
        Thread[] hilo = new Thread[6];
        hilo[0] = new Thread(brazo, "Brazo");
        hilo[1] = new Thread(empaque1, "Empaquetador 1");
        hilo[2] = new Thread(empaque2, "Empaquetador 2");
        hilo[3] = new Thread(hornoA, "Horno A");
        hilo[4] = new Thread(hornoB, "Horno B");
        hilo[5] = new Thread(hornoC, "Horno C");
        for (int i = 0; i < 6; i++) {
            hilo[i].start();
        }
    }
}
