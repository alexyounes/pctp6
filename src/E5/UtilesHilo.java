package E5;

import E4.*;
import E3.*;

/**
 *
 * @author Alejandro Younes
 */
public class UtilesHilo {

    public static String nombre() {
        return Thread.currentThread().getName();
    }

    public static void simularActividad(int milisegundos) {
        try {
            Thread.sleep(milisegundos);
        } catch (InterruptedException ex) {
        }
    }
}
