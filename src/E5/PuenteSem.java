package E5;

import static E3.UtilesHilo.nombre;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class PuenteSem {

    private final Semaphore[] semEntrada = {
        new Semaphore(0, true), // Entrada Sur
        new Semaphore(0, true) // Entrada Norte
    };
    private final Semaphore[] semSalida = {
        new Semaphore(0, true), // Salida Sur
        new Semaphore(0, true) // Salida Norte
    };

    private final Semaphore semMutex = new Semaphore(1);
    private int senAct = -1; // -1: Ninguno, 0: Sur, 1:Norte
    private int ocupando = 0;
    private int numFin = 0;
    private int esperando = 0;
    private int numOrden = -1;
    private int colaEntrada = 0;

    public int entrarCocheDelNorte() throws InterruptedException {
        return entrarCoche(1, 0);
    }

    public void salirCocheDelSur(int numSalida) throws InterruptedException {
        salirCoche(numSalida, 0, 1);
    }

    public int entrarCocheDelSur() throws InterruptedException {
        return entrarCoche(0, 1);
    }

    public void salirCocheDelNorte(int numSalida) throws InterruptedException {
        salirCoche(numSalida, 1, 0);
    }

    private int entrarCoche(int sentido, int opuesto) throws InterruptedException {
        semMutex.acquire();
        if (senAct != opuesto) { // Si no vienen del sentido opuesto
            senAct = sentido; // Indica su sentido
            semMutex.release();
        } else {
            colaEntrada++;
            semMutex.release();
            semEntrada[1].acquire();
        }
        semMutex.acquire();
        ocupando++; // Ocupando puente
        if (sentido == 0) {
            System.out.println(nombre() + "cruzando hacia SUR");
        } else {
            System.out.println(nombre() + "cruzando hacia NORTE");
        }
        numOrden++; // Indica su orden de salida
        semMutex.release();
        return numOrden;
    }

    private void salirCoche(int numSalida, int sentido, int opuesto) throws InterruptedException {
        semMutex.acquire();
        if (numSalida != numFin) { // Si orden salida != llegada
            esperando++; // Indica que llego antes
            while (numSalida != numFin) {
                semMutex.release();
                semSalida[sentido].acquire(); // Espera que llegue el pasado
                semMutex.acquire();
            }
        }
        if (esperando > 0) { // Si hay autos que llegaron antes
            esperando--; // Indica que va a sacar uno
            semSalida[sentido].release(); // Libera el primero
        }
        ocupando--; // Desocupa puente
        numFin++; // Indica que paso el puente
        if (ocupando == 0) { // Si es el ultimo auto en el puente
            senAct = -1; // Reinicia sentido
            numOrden = -1; // Reinicia orden salida
            numFin = 0; // Reinicia finalizados
            semEntrada[opuesto].release(colaEntrada); // Libera al sentido opuesto
            colaEntrada = 0; // Reinicia cola del sentido opuesto
        }
        if (sentido == 0) {
            System.out.println(nombre() + "sale del SUR");
        } else {
            System.out.println(nombre() + "sale del NORTE");
        }
        semMutex.release();
    }
}
