package E2;

/**
 *
 * @author Alejandro Younes
 */
public class Jubilada extends Persona {

    public Jubilada(GestorSala sala) {
        super(sala);
    }

    @Override
    public void run() {
        System.out.println(nombre() + ": quiere entrar");
        sala.entrarSalaJubilado();
        System.out.println(nombre() + ": entra y ve obra");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        sala.salirSala();
        System.out.println(nombre() + ": se va");
    }
}
