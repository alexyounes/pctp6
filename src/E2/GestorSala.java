package E2;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class GestorSala {

    private final int tUmbral = 30;
    private final int cantNormal = 50;
    private final int cantUmbral = 35;
    private int limiteActual;
    private int cantActual;
    private int tActual;

    private int jubiladasEsperando = 0;
    private final ReentrantLock cerJubiladasEsperando = new ReentrantLock();
    private final Object monJubiladas = new Object();

    public GestorSala() {
        cantActual = 0;
        limiteActual = cantNormal;
        tActual = 20;
    }

    // se invoca cuando una persona quiere entrar en la sala.
    void entrarSala() {
        synchronized (this) {
            while (cantActual > limiteActual) {
                try {
                    this.wait();
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }
        synchronized (monJubiladas) {
            while (jubiladasEsperando > 0) {
                try {
                    monJubiladas.wait();
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }
        synchronized (this) {
            cantActual++;
        }
    }

    // se invoca cuando una persona jubilada quiere entrar en la sala.
    void entrarSalaJubilado() {
        cerJubiladasEsperando.lock();
        jubiladasEsperando++;
        cerJubiladasEsperando.unlock();
        synchronized (this) {
            while (cantActual > limiteActual) {
                try {
                    this.wait();
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
            cantActual++;
        }
        cerJubiladasEsperando.lock();
        jubiladasEsperando--;
        cerJubiladasEsperando.unlock();
        synchronized (monJubiladas) {
            monJubiladas.notifyAll();
        }
    }

    // se invoca cuando una persona, jubilada o no, quiere salir de la sala.
    synchronized void salirSala() {
        cantActual--;
        this.notifyAll();
    }

    // lo invoca la hebra que mide la temperatura de la sala para indicar el último valor medido.
    synchronized void notificarTemperatura(int temperatura) {
        tActual = temperatura;
        System.out.println("tActual: " + tActual);
        if (tActual > tUmbral) {
            limiteActual = cantUmbral;
        } else {
            limiteActual = cantNormal;
        }
    }
}
