package E2;

import java.util.Random;

/**
 *
 * @author Alejandro Younes
 */
public class Termometro implements Runnable {

    private final GestorSala sala;
    private final Random r;

    public Termometro(GestorSala sala) {
        this.sala = sala;
        this.r = new Random();
    }

    private void actualizarTemperatura() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        int nuevaTemperatura = 20 + r.nextInt(20);
        sala.notificarTemperatura(nuevaTemperatura);
    }

    @Override
    public void run() {
        while (true) {
            actualizarTemperatura();
        }
    }
}
