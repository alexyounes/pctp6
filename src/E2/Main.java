package E2;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        final int NPERSONAS = 100;
        final int NJUBILADAS = 80;
        GestorSala sala = new GestorSala();
        Termometro termometro = new Termometro(sala);
        Thread hTermometro = new Thread(termometro, "Termometro");
        hTermometro.start();
        Persona[] personas = new Persona[NPERSONAS];
        Thread[] hilos = new Thread[NPERSONAS];
        for (int i = 0; i < NJUBILADAS; i++) {
            personas[i] = new Jubilada(sala);
            hilos[i] = new Thread(personas[i], "Jubilada " + i);
            hilos[i].start();
        }
        for (int i = NJUBILADAS; i < NPERSONAS; i++) {
            personas[i] = new Persona(sala);
            hilos[i] = new Thread(personas[i], "Persona " + (i - NJUBILADAS));
            hilos[i].start();
        }
    }
}
