package E2;

/**
 *
 * @author Alejandro Younes
 */
public class Persona implements Runnable {

    final GestorSala sala;

    public Persona(GestorSala sala) {
        this.sala = sala;
    }

    @Override
    public void run() {
        System.out.println(nombre() + ": quiere entrar");
        sala.entrarSala();
        System.out.println(nombre() + ": entra y ve obra");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        sala.salirSala();
        System.out.println(nombre() + ": se va");
    }

    String nombre() {
        return Thread.currentThread().getName();
    }
}
