package E4;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        final int numSol = 10;
        Comedor comedor = new Comedor();
        Soldado soldado = new Soldado(comedor);
        Thread[] hiloSol = new Thread[numSol];
        for (int i = 0; i < numSol; i++) {
            hiloSol[i] = new Thread(soldado, "Soldado " + i + ": ");
            hiloSol[i].start();
        }
    }
}
