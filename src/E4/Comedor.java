package E4;

import static E4.UtilesHilo.nombre;
import static E4.UtilesHilo.simularActividad;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Comedor {

    private final Semaphore mostradorAlmuerzo = new Semaphore(5);
    private final Semaphore abridor = new Semaphore(10);
    private final Semaphore mostradorPostre = new Semaphore(3);

    public void obtenerBandejaComida() {
        System.out.println(nombre() + "quiere bandeja comida");
        try {
            mostradorAlmuerzo.acquire();
        } catch (InterruptedException ex) {
        }
        System.out.println(nombre() + "se sirve comida");
        simularActividad(4000);
        System.out.println(nombre() + "obtuvo bandeja comida");
        mostradorAlmuerzo.release();
    }

    public void abrirBotella() {
        System.out.println(nombre() + "quiere abrir botella");
        try {
            abridor.acquire();
        } catch (InterruptedException ex) {
        }
        System.out.println(nombre() + "abre botella");
        simularActividad(1000);
        System.out.println(nombre() + "abrio botella");
        abridor.release();
    }

    public void obtenerPostre() {
        System.out.println(nombre() + "quiere abrir botella");
        try {
            mostradorPostre.acquire();
        } catch (InterruptedException ex) {
        }
        System.out.println(nombre() + "se sirve postre");
        simularActividad(2000);
        System.out.println(nombre() + "obtuvo postre");
        mostradorPostre.release();
    }
}
