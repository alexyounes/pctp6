package E4;

import static E4.UtilesHilo.nombre;
import static E4.UtilesHilo.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Soldado implements Runnable {
    Comedor comedor;

    public Soldado(Comedor comedor) {
        this.comedor = comedor;
    }
    
    private void logica(){
        comedor.obtenerBandejaComida();
        comedor.abrirBotella();
        comedor.obtenerPostre();
        System.out.println(nombre() + "come");
        simularActividad(5000);
        System.out.println(nombre() + "deja bandeja en cocina");
    }

    @Override
    public void run() {
        logica();
    }
}